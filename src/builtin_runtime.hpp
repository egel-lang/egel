#ifndef BUILTIN_RUNTIME_HPP
#define BUILTIN_RUNTIME_HPP

#include "runtime.hpp"

std::vector<VMObjectPtr> builtin_runtime(VM* vm);

#endif
